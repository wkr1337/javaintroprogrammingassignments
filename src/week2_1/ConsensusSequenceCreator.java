/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */
package week2_1;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {

    /**
     * testing main.
     *
     * @param args
     */
    public static void main(String[] args) {
        String[] sequences = new String[5];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";
        sequences[4] = "GAAC";
        sequences[4] = "GAAC";
        sequences[4] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
//        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     *
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {

        for (int j = 0; j < sequences[1].length(); j++) {
            String letters = "";
            for (String sequence : sequences) {
                letters += (sequence.charAt(j));
            }

            System.out.println("letters = " + letters);
            if (letters.contains("A") && letters.contains("T")) {
                System.out.println("v");
            }
            System.out.println("sdfga= ");
        }
        return "";
    }

    public Map iupac() {
        Map<String, String> iupacEncodingMap = new HashMap<String, String>();
        iupacEncodingMap.put("A", "A");
        iupacEncodingMap.put("C", "C");
        iupacEncodingMap.put("G", "G");
        iupacEncodingMap.put("T", "T");
        iupacEncodingMap.put("U", "U");

        iupacEncodingMap.put("W", "AT");
        iupacEncodingMap.put("S", "CG");
        iupacEncodingMap.put("M", "AC");
        iupacEncodingMap.put("K", "GT");
        iupacEncodingMap.put("R", "AG");
        iupacEncodingMap.put("Y", "CT");
        iupacEncodingMap.put("B", "CGT");
        iupacEncodingMap.put("D", "AGT");
        iupacEncodingMap.put("H", "ACT");
        iupacEncodingMap.put("V", "ACG");
        iupacEncodingMap.put("N", "ACGT");
        iupacEncodingMap.put("Z", "0");

        return null;

    }
}
